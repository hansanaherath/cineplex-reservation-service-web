import React, { Component } from "react";
import { Form } from "react-bootstrap";
import InputComponent from "../commonComponents/InputComponent";

class SetupNewProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            key: {}
        }
    }
    componentDidMount() {
        this.setState({
            data: [...this.props.data],
            key: { ...this.props.name }
        })
    }

    onChangeFunc = (e) => {
        let data = [...this.state.data]
        let obj = {}
        data.forEach(element => {
            if (element.id === e.target.id) {
                element.value = e.target.value
            }
        });
        this.setState({
            data: data
        })
        obj[this.state.key.key] = data
        this.props.addDataToRedux(obj, this.state.key.key);
    }

    render() {
        let form = [...this.state.data]
        let data = form.length > 0 && form.map((item, i) => {
            return (
                <div key={i} className="section-body">
                    <Form.Group controlId={item.productcode}>
                        <Form.Label>{item.label}</Form.Label><span><i className="fas fa-star-of-life required-star"></i></span>
                        <InputComponent
                            id={item.id}
                            type={item.type}
                            name={item.productcode}
                            placeholder={item.placeholder}
                            value={item.value}
                            onChange={(e) => this.onChangeFunc(e)}
                            options={item.hasOwnProperty("options") ? item.options : null}
                            checked={item.hasOwnProperty("options") ? (item.value == item.options.id ? true : false) : null}
                        />
                    </Form.Group>
                </div>
            )
        }, this);

        return (
            <div>
                {data}
            </div>
        )
    }
}

export default SetupNewProduct;