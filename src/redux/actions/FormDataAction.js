export const FormDataAction = data => {

  return (dispatch, getState) => {
    dispatch({
      type: "DATALOADING_TEXTBOX",
      payload: {
        data: data
      }
    });
  };
};

export const ChangeReduxFormAction = data => {

  return (dispatch, getState) => {
    dispatch({
      type: "CHANGE_REDUX_FORM",
      payload: {
        data: data
      }
    });
  };
};

export const ClearFormAction = data => {

  return (dispatch, getState) => {
    dispatch({
      type: "CLEAR_FORM",
      payload: {
        data: data
      }
    });
  };
};
