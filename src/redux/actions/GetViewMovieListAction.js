import axios from 'axios';
import { LOOKUP_CONTROLLER_URL } from './settings';

export const GetViewMovieListAction = (val, id) => {
    let url = LOOKUP_CONTROLLER_URL + "/api/v1/view/movies"
    return (dispatch, getState) => {
        dispatch({
            type: 'FETCHING_VIEW_MOVIE_LIST',
            payload: {
                fetching: true,
                data: {}
            }
        });

        axios({
            method: 'GET',
            url: url,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json;charset=UTF-8',
            }
        }).then(response => {
            dispatch({
                type: 'VIEW_MOVIE_LIST_SUCCESS',
                payload: {
                    fetchingData: false,
                    data: response.data
                }
            });
        }).catch(e => {
            dispatch({
                type: 'VIEW_MOVIE_LIST_FAILED',
                payload: {
                    fetchingData: false,
                    data: e
                }
            });
        });
    }
}

export const resetGetViewMovieListAction = () => {
    return (dispatch) => {
        dispatch({
            type: 'VIEW_MOVIE_LIST_RESET',
            payload: {
                fetching: false,
                data: {}
            }
        });
    }
}