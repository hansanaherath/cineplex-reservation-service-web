import axios from 'axios';
import { LOOKUP_CONTROLLER_URL } from './settings';

export const GetShowTimeListAction = (array) => {

    let url = LOOKUP_CONTROLLER_URL + "/api/v1/showTimeOptions"

    return (dispatch, getState) => {
        dispatch({
            type: 'FETCHING_ALL_SHOWTIME_LIST', 
            payload: { 
                fetching: true, 
                data: {} 
            } 
        });

        axios({
            method: 'GET',
            url: url,
            headers:{
              'Access-Control-Allow-Origin':'*',
              'Content-Type': 'application/json;charset=UTF-8',
          }
          }).then(response => {
            dispatch({ 
                type: 'FETCHING_ALL_SHOWTIME_LIST_SUCCESS', 
                payload: { 
                    fetchingData: false, 
                    data: response.data 
                } 
            });
        }).catch(e => {
            dispatch({ 
                type: 'FETCHING_ALL_SHOWTIME_LIST_FAILED', 
                payload: { 
                    fetchingData: false, 
                    data: e 
                } 
            });
        });
    }
}

export const resetGetAllShowTimesAction = () => {
    return (dispatch) => {
      dispatch({
        type: 'FETCHING_ALL_SHOWTIME_LIST_RESET',
        payload: {
          fetching: false,
          data: {}
        }
      });
    }
  }