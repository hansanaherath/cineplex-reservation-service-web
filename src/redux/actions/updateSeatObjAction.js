import axios from 'axios';
import { LOOKUP_CONTROLLER_URL } from './settings';

export const updateSeatObjAction = (id) => {

    let url = LOOKUP_CONTROLLER_URL + "/api/v1/view/updateSeat/"+id;

    return (dispatch, getState) => {
        dispatch({
            type: 'FETCHING_UPDATE_SEAT', 
            payload: { 
                fetching: true, 
                data: {} 
            } 
        });

        axios({
            method: 'PUT',
            url: url,
            headers:{
              'Access-Control-Allow-Origin':'*',
              'Content-Type': 'application/json;charset=UTF-8',
          },
          data : id,
          }).then(response => {
            dispatch({ 
                type: 'SAVING_UPDATE_SEAT_SUCCESS', 
                payload: { 
                    fetchingData: false, 
                    data: response.data 
                } 
            });
        }).catch(e => {
          alert("Data Saving Faild.")
            dispatch({ 
                type: 'SAVING_UPDATE_SEAT_FAILED', 
                payload: { 
                    fetchingData: false, 
                    data: e 
                } 
            });
        });
    }
}

export const resetUpdateSeatObjAction = () => {
    return (dispatch) => {
      dispatch({
        type: 'SAVING_UPDATE_SEAT_RESET',
        payload: {
          fetching: false,
          data: {}
        }
      });
    }
  }