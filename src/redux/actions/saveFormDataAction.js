import axios from 'axios';
import { LOOKUP_CONTROLLER_URL } from './settings';

export const saveFormDataAction = (array,method,reqpara) => {

    let url = LOOKUP_CONTROLLER_URL + reqpara;

    return (dispatch, getState) => {
        dispatch({
            type: 'FETCHING_SAVING_SETUP_MOVIE', 
            payload: { 
                fetching: true, 
                data: {} 
            } 
        });

        axios({
            method: method,
            url: url,
            headers:{
              'Access-Control-Allow-Origin':'*',
              'Content-Type': 'application/json;charset=UTF-8',
          },
          data : array,
          }).then(response => {
            dispatch({ 
                type: 'SAVING_SAVING_SETUP_MOVIE_SUCCESS', 
                payload: { 
                    fetchingData: false, 
                    data: response.data 
                } 
            });
        }).catch(e => {
          alert("Data Saving Faild.")
            dispatch({ 
                type: 'SAVING_SAVING_SETUP_MOVIE_FAILED', 
                payload: { 
                    fetchingData: false, 
                    data: e 
                } 
            });
        });
    }
}

export const resetSavingMovieList = () => {
    return (dispatch) => {
      dispatch({
        type: 'SAVE_SAVING_SETUP_MOVIE_RESET',
        payload: {
          fetching: false,
          data: {}
        }
      });
    }
  }