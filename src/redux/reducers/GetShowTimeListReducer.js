const GetShowTimeListReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_ALL_SHOWTIME_LIST':
            return Object.assign({}, state, {
                fetching: true,
                showTimeList: []
            });

        case 'FETCHING_ALL_SHOWTIME_LIST_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                showTimeList: action.payload.data

            });

        case 'FETCHING_ALL_SHOWTIME_LIST_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                showTimeList: []
            });
        case 'FETCHING_ALL_SHOWTIME_LIST_RESET':
            return Object.assign({}, state, {
                fetching: false,
                showTimeList: []
            });
        default:
            return state;
    }

}

export default GetShowTimeListReducer;