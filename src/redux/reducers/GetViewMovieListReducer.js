const GetViewMovieListReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_VIEW_MOVIE_LIST':
            return Object.assign({}, state, {
                fetching: true,
                fetchViewMovieList: {}
            });

        case 'VIEW_MOVIE_LIST_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                fetchViewMovieList: action.payload.data

            });

        case 'VIEW_MOVIE_LIST_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                fetchViewMovieList: {}
            });
        case 'VIEW_MOVIE_LIST_RESET':
            return Object.assign({}, state, {
                fetching: false,
                fetchViewMovieList: {}
            });
        default:
            return state;
    }

}

export default GetViewMovieListReducer;