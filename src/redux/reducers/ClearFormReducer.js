const ClearFormReducer = (state = {}, action) => {
  switch (action.type) {
    case "CLEAR_FORM":      
      return Object.assign({}, state, {        
        "form": action.payload.data
      });

    default:
      return state;
  }
};

export default ClearFormReducer;
