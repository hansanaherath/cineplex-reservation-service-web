const FormDataReducer = (state = {}, action) => {
  switch (action.type) {
    case "DATALOADING_TEXTBOX":      
      return Object.assign({}, state, {        
        "formData": action.payload.data
      });

    default:
      return state;
  }
};

export default FormDataReducer;
