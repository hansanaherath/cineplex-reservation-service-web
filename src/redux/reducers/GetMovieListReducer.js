const GetMovieListReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_MOVIE_LIST':
            return Object.assign({}, state, {
                fetching: true,
                fetchMovieList: {}
            });

        case 'FETCHING_MOVIE_LIST_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                fetchMovieList: action.payload.data

            });

        case 'FETCHING_MOVIE_LIST_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                fetchMovieList: {}
            });
        case 'FETCHING_MOVIE_LIST_RESET':
            return Object.assign({}, state, {
                fetching: false,
                fetchMovieList: {}
            });
        default:
            return state;
    }

}

export default GetMovieListReducer;