import { combineReducers } from "redux";
import FormDataReducer from "./FormDataReducer";
import saveFormDataReducer from "./saveFormDataReducer";
import ChangeReduxFormReducer from "./ChangeReduxFormRducer";
import GetMovieListReducer from "./GetMovieListReducer";
import updateSeatObjReducer from "./updateSeatObjReducer";
import GetViewMovieListReducer from "./GetViewMovieListReducer";
import GetShowTimeListReducer from "./GetShowTimeListReducer";


export default combineReducers(
  // This would produce the following state object
  {
    form : ChangeReduxFormReducer,
    globalData : FormDataReducer,
    movieList : saveFormDataReducer,
    fetchMovieList : GetMovieListReducer,
    updateSeatObj : updateSeatObjReducer,
    fetchViewMovieList : GetViewMovieListReducer,
    showTimeList : GetShowTimeListReducer
  }
);
