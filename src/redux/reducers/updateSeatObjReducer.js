const updateSeatObjReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_UPDATE_SEAT':
            return Object.assign({}, state, {
                fetching: true,
                updateSeatObj: {}
            });

        case 'SAVING_UPDATE_SEAT_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                updateSeatObj: action.payload.data

            });

        case 'SAVING_UPDATE_SEAT_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                updateSeatObj: {}
            });
        case 'SAVING_UPDATE_SEAT_RESET':
            return Object.assign({}, state, {
                fetching: false,
                updateSeatObj: {}
            });
        default:
            return state;
    }

}

export default updateSeatObjReducer;