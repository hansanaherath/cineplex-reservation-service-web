const saveFormDataReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_SAVING_SETUP_MOVIE':
            return Object.assign({}, state, {
                fetching: true,
                movieList: {}
            });

        case 'SAVING_SAVING_SETUP_MOVIE_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                movieList: action.payload.data

            });

        case 'SAVING_SAVING_SETUP_MOVIE_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                movieList: {}
            });
        case 'SAVE_SAVING_SETUP_MOVIE_RESET':
            return Object.assign({}, state, {
                fetching: false,
                movieList: {}
            });
        default:
            return state;
    }

}

export default saveFormDataReducer;