const ChangeReduxFormReducer = (state = {}, action) => {
  switch (action.type) {
    case "CHANGE_REDUX_FORM":      
      return Object.assign({}, state, {   
             
        [action.payload.data.key]: action.payload.data.value
      });

    default:
      return state;
  }
};

export default ChangeReduxFormReducer;
