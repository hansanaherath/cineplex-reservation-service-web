import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { ChangeReduxFormAction } from '../../../redux/actions/FormDataAction';
import { connect } from 'react-redux';

class RadioButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radio: 'Y'
        }
    }
    componentDidMount0() {
        let obj = {
            "key": "radio",
            "value": 'Y'
        }
        this.props.ChangeReduxFormAction(obj);
    }

    onChangeRadio = (e) => {
        this.setState({
            radio: e.target.value
        })

        let obj = {
            "key": "radio",
            "value": e.target.value
        }
        this.props.ChangeReduxFormAction(obj);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.form && props.form.radio && props.form.radio !== state.radio) {
            return ({
                radio: props.form.radio
            })
        }
        if (!props.form.hasOwnProperty("radio")) {
            return ({
                radio: "Y"
            })
        }
    }

    render() {

        return (
            <div>
                <Row>
                    <Col lg={4}>
                        <label>Is Movie Active</label>
                    </Col>
                    <Col lg={8}>
                        <div className="form-radio-button-main"> <input type="radio" value="Y" name="status" id="status_yes" checked={this.state.radio === "Y"} onChange={(e) => { this.onChangeRadio(e) }} /> <label for="status_yes">Yes</label></div>
                        <div className="form-radio-button-main"> <input type="radio" value="N" name="status" id="status_no" checked={this.state.radio === "N"} onChange={(e) => { this.onChangeRadio(e) }} /> <label for="status_no">No</label></div>
                    </Col>
                </Row>
            </div>
        )
    }
}
const mapDipatchToProps = dispatch => {
    return {
        ChangeReduxFormAction: (obj) => {
            dispatch(ChangeReduxFormAction(obj));
        }
    }
}

const mapStateToProps = state => {
    return {

    }
}

export default connect(mapStateToProps, mapDipatchToProps)(RadioButtonComponent);