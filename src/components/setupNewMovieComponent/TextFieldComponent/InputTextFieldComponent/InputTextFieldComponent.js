import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { ChangeReduxFormAction } from '../../../../redux/actions/FormDataAction';
import { connect } from 'react-redux';

class InputTextFieldComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value : ''
        }
    }
    handleOnChange = (e) =>{
        this.setState({value :e.target.value})
        let obj ={
            "key" : [e.target.id],
            "value" :e.target.value
        }
        this.props.ChangeReduxFormAction(obj);
    } 

    static getDerivedStateFromProps(props, state){
        if(props.form && props.form[props.field.id] !== state.value){
            return ({
                value : props.form[props.field.id]
            })
        }
    }

    render() {
        const { name, type, id, value, placehoder, className } = this.props.field
        return (
            <Row>
                <Col lg={4}>
                    <label>{name}</label>
                </Col>
                <Col lg={8}>
                    {type === 'text' ?
                        <input name={name} id={id} value={this.state.value} placeholder={placehoder} className={className} onChange={(e) => {this.handleOnChange(e)}}></input> :
                        <textarea name={name} id={id} value={this.state.value} placeholder={placehoder} className={className} onChange={(e) => {this.handleOnChange(e)}}></textarea>
                    }

                </Col>
            </Row>
        )
    }
}
const mapDipatchToProps = dispatch => {
    return {
        ChangeReduxFormAction : (obj)=>{
            dispatch(ChangeReduxFormAction(obj));
        } 
    }
  }
  
  const mapStateToProps = state => {
    return {
     
    }
  }
  
export default connect(mapStateToProps, mapDipatchToProps)(InputTextFieldComponent);

