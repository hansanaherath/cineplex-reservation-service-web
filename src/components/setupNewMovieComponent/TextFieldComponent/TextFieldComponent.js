import React, { Component } from 'react';
import InputTextFieldComponent from './InputTextFieldComponent/InputTextFieldComponent';

class TextFieldComponent extends Component {
    render() {

        return (
            <div>
                {this.props.textFieldComponent.length > 0 ?this.props.textFieldComponent.map(field => (
                    <InputTextFieldComponent
                        field={field} 
                        handleOnChange={this.props.handleOnChange}
                        form={this.props.form}/>
                )): null}

            </div>
        )
    }
}


export default TextFieldComponent;