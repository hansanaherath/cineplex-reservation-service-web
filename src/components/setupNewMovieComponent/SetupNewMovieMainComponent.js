import React, { Component } from "react";
import TextFieldComponent from "./TextFieldComponent/TextFieldComponent";
import TypeHeadComponent from "./TypeHeadComponent/TypeHeadComponent";
import RadioButtonComponent from "./RadioButtonComponent/RadioButtonComponent";
import AddButtonComponent from "./AddButtonComponent/AddButtonComponent";
import SaveButtonComponent from "./SavingButtonComponent/SaveButtonComponent";
import LoadTableComponent from "./tableComponent/LoadTableComponent";
import { Container } from "react-bootstrap";
import { connect } from "react-redux";
import { saveFormDataAction, resetSavingMovieList } from "../../redux/actions/saveFormDataAction";
import { ChangeReduxFormAction } from "../../redux/actions/FormDataAction";
import { GetMovieListAction, resetFetchingMovies } from "../../redux/actions/GetMovieListAction";

class SetupNewMovie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {},
            gridArray: [],
            deleteArray: [],
            saveUpdatedObj: [],
            saveNewObj: []
        }
    }

    savingArray = (array, saveUpdatedObj, saveNewObj) => {
        this.setState({
            gridArray: array,
            saveUpdatedObj: saveUpdatedObj,
            saveNewObj: saveNewObj
        })
    }

    remove = (gridArray, deleteArray) => {
        this.setState({
            gridArray: gridArray,
            deleteArray: deleteArray
        })
    }

    save = () => {
        let updateArray = [...this.state.saveUpdatedObj]
        let deleteArray = [...this.state.deleteArray]
        let saveArray = [...this.state.saveNewObj]
        let updateIds = this.makeIds(updateArray)
        let deleteIds = this.makeIds(deleteArray)
        if (saveArray.length > 0) {
            this.props.saveFormDataAction(saveArray, "POST", "/api/v1/movies")
        }
        if (updateArray.length > 0) {
            this.props.saveFormDataAction(updateArray, "PUT", ("/api/v1/movies/" + updateIds))
        }
        if (deleteArray.length > 0) {
            this.props.saveFormDataAction(deleteArray, "DELETE", ("/api/v1/movies/" + deleteIds))
        }
    }

    makeIds = (updateArray) => {
        let ids = ""
        updateArray.forEach((element, i) => {
            if (i === (updateArray.length - 1)) {
                ids += element.id
            } else {
                ids += element.id + ","
            }
        });
        return ids;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.fetchMovieList && nextProps.fetchMovieList.data) {
            this.props.resetFetchingMovies();
        }
        return true;
    }

    static getDerivedStateFromProps(props, state) {
        if (props.fetchMovieList && props.fetchMovieList.data && props.fetchMovieList.data.length >= 0) {
            return ({
                gridArray: props.fetchMovieList.data
            })
        }
    }

    edit = (obj, index, array) => {
        let showtimeArray = typeof obj !== undefined && obj.hasOwnProperty("showTimesEntityList") ? obj.showTimesEntityList : obj.showTimeEntityList
        let seatsArray = showtimeArray[0].hasOwnProperty("seatList") ? showtimeArray[0].seatList : showtimeArray[0].seatsEntityList
        let updatedShowTimeArray = []
        let updatedSeatsArray = []
        showtimeArray.forEach(element => {
            let showtimeObj = {
                "id": element.showTime,
                "value": element.showTimeOptionValue,
                "entityId" : element.id
            }
            updatedShowTimeArray.push(showtimeObj);
        });

        seatsArray.forEach(element => {
            let seatObj = {
                "customOption": true,
                "id": element.id,
                "label": element.seatNumber
            }
            updatedSeatsArray.push(seatObj);
        });

        let objArray = [{ "key": "id", "value": obj.id }, { "key": "moviename", "value": obj.name }, { "key": "description", "value": obj.description }, { "key": "showtime", "value": updatedShowTimeArray }, { "key": "seats", "value": updatedSeatsArray }, { "key": "radio", "value": obj.status }, { "key": "index", "value": index }]

        objArray.forEach(element => {
            this.props.editFormData(element)
        });
    }

    render() {
        const { gridArray, deleteArray } = this.state;

        return (
            <div>
                <Container fluid>
                    <div className="add-movie-form-main">
                        <TextFieldComponent
                            textFieldComponent={this.props.formData.textFieldComponent ? this.props.formData.textFieldComponent : []}
                            form={this.props.form}
                        />

                        <TypeHeadComponent
                            typehead={this.props.formData.typehead ? this.props.formData.typehead : []}
                            form={this.props.form} />

                        <RadioButtonComponent
                            form={this.props.form} />
                    </div>
                    <AddButtonComponent
                        savingArray={this.savingArray}
                        gridArray={gridArray}
                    />

                    <LoadTableComponent
                        gridArray={gridArray}
                        deleteArray={deleteArray}
                        remove={this.remove}
                        edit={this.edit}
                    />

                    <SaveButtonComponent
                        save={this.save} />
                </Container>
            </div>);
    }
}

const mapDipatchToProps = dispatch => {
    return {
        saveFormDataAction: (obj, method, param) => {
            dispatch(saveFormDataAction(obj, method, param));
        },
        editFormData: (obj) => {
            dispatch(ChangeReduxFormAction(obj));
        },
        GetMovieListAction: () => {
            dispatch(GetMovieListAction());
        },
        resetSavingMovieList: () => {
            dispatch(resetSavingMovieList());
        },
        resetFetchingMovies: () => {
            dispatch(resetFetchingMovies());
        }
    }
}

const mapStateToProps = state => {
    return {
        formData: state.globalData.formData ? state.globalData.formData : {},
        form: state.form,
        movieList: state.movieList ? state.movieList : {}
    }
}

export default connect(mapStateToProps, mapDipatchToProps)(SetupNewMovie);