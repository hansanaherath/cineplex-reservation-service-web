import React, { Component } from "react";
import { Col, Button } from "react-bootstrap";
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import { ChangeReduxFormAction } from "../../../redux/actions/FormDataAction";
import { FaPlus } from "react-icons/fa";

class AddButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    /* notifyError = (message) => {
        toast.error(message, {
            position: "top-right",
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: false
        });
    } */

    addDataToGrid = () => {
        let formObj = { ...this.props.formObj }
        let gridArray = [...this.props.gridArray];
        let isValidate = this.dataValidation(formObj);
        let saveNewObj = [];
        let saveUpdatedObj = [];

        if (!formObj.hasOwnProperty("radio")) {
            formObj["radio"] = "Y"
        }

        if (!isValidate) {
            alert("validation error");
        } else {
            let seatArray = [];
            formObj.seats.forEach(element => {
                let seatObj = {
                    "id" : isNaN(element.id) && element.id.includes("new") ? null :element.id,
                    "isReserved": "N",
                    "status": "Y",
                    "seatNumber": Number(element.label)
                }
                seatArray.push(seatObj);
            });

            let showTimeArray = [];
            formObj.showtime.forEach(ele => {
                let showTimeObj = {
                    "id" : ele.hasOwnProperty("entityId") ? ele.entityId : null,
                    "showTime": ele.id,
                    "status": "Y",
                    "seatsEntityList": seatArray,
                    "showTimeOptionValue" : ele.value
                }
                showTimeArray.push(showTimeObj);
            });

            let savingObj = {
                "id": formObj.hasOwnProperty("id") ?  formObj.id:null,
                "name": formObj.moviename,
                "description": formObj.description,
                "status": formObj.radio,
                "showTimesEntityList": showTimeArray
            }

            let isEqualName = false;
            if (gridArray.length > 0) {
                gridArray.forEach(obj => {
                    if (obj.name === savingObj.name && !formObj.hasOwnProperty("index")) {
                        isEqualName = true;
                        alert("same movie");
                    } 
                })

                if (formObj.hasOwnProperty("index")) {
                    gridArray[formObj.index] = savingObj;
                    if(savingObj.id !== null ){
                        saveUpdatedObj.push(savingObj);
                    }else{
                        saveNewObj.push(savingObj)
                    }
                }else if (!isEqualName) {
                    gridArray.push(savingObj);
                    saveNewObj.push(savingObj)
                }
            } else {
                gridArray.push(savingObj);
                saveNewObj.push(savingObj)
            }


            let clearObj = [{ "key": "moviename", "value": "" }, { "key": "description", "value": "" }, { "key": "showtime", "value": [] }, { "key": "seats", "value": [] }, { "key": "radio", "value": "Y" }]

            clearObj.forEach(dataObj => {
                this.props.clearFormData(dataObj)
            });

            this.props.savingArray(gridArray,saveUpdatedObj,saveNewObj);
        }

    }


    dataValidation = (obj) => {
        let isValidate = true;
        if (!obj.hasOwnProperty("description") ||
            (obj.hasOwnProperty("description") && (obj.description === "" || obj.description === null))) {
            isValidate = false;
            return isValidate;
        }

        if (!obj.hasOwnProperty("moviename") ||
            (obj.hasOwnProperty("moviename") && (obj.moviename === "" || obj.moviename === null))) {
            isValidate = false;
            return isValidate;
        }

        if (!obj.hasOwnProperty("seats") ||
            (obj.hasOwnProperty("seats") && obj.seats.length <= 0)) {
            isValidate = false;
            return isValidate;
        }

        if (!obj.hasOwnProperty("showtime") ||
            (obj.hasOwnProperty("showtime") && obj.showtime.length <= 0)) {
            isValidate = false;
            return isValidate;
        }
        return isValidate;
    }

    render() {
        const { gridArray, form, editObj } = this.props
        return (
            <Col>
                <div className="section-body add-button-main">
                    {this.props.formObj.hasOwnProperty("id") ?
                        <Button className="controller-btn btn btn-primary custom-btn" onClick={() => { this.addDataToGrid() }} ><FaPlus className="add-button-icon"/>Done</Button> :
                        <Button className="controller-btn btn btn-primary custom-btn" onClick={() => { this.addDataToGrid() }} ><FaPlus className="add-button-icon"/>Add</Button>
                    }
                </div>
            </Col>

        )
    }
}
const mapDipatchToProps = dispatch => {
    return {
        clearFormData: (obj) => {
            dispatch(ChangeReduxFormAction(obj));
        }
    }
}

const mapStateToProps = state => {
    return {
        formObj: state.form ? state.form : {}
    }
}
export default connect(mapStateToProps, mapDipatchToProps)(AddButtonComponent);
