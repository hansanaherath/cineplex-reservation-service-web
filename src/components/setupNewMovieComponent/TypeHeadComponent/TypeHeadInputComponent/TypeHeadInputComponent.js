import React, { Component } from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import { Col, Row } from 'react-bootstrap';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { connect } from 'react-redux';
import { ChangeReduxFormAction } from '../../../../redux/actions/FormDataAction';

class TypeHeadInputComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected : []
        }
    }
    handleOnchange = (value) => {
        let selectedArray = [...this.state.selected]
        let lastIndex = value.selected.length -1
        let datavalue = value.selected.length > 0 ? value.selected[lastIndex].label : null;

        if(!isNaN(Number(datavalue)) && this.props.typehead.id === 'seats'){
            selectedArray = value.selected;
        }else if(this.props.typehead.id === 'showtime'){
            selectedArray = value.selected;
        }

        this.setState({
            selected : selectedArray
        });
        let obj ={
            "key" : this.props.typehead.id,
            "value" :selectedArray
        }
        this.props.ChangeReduxFormAction(obj);
    }

    static getDerivedStateFromProps(props, state){
        if(props.form && props.form[props.typehead.id] !== state.value){
            return ({
                selected : props.form[props.typehead.id]
            })
        }
    }

    render() {
        const { id, options, selected, placeholder, filter, label, key,allowNew } = this.props.typehead
        return (
            <Row >
                <Col lg={4}>
                    <label>{label}</label>
                </Col>
                <Col lg={8}>
                {allowNew ? 
                    <Typeahead
                        key={key}
                        id={id}
                        allowNew = {allowNew}
                        clearButton
                        bsSize="small"
                        multiple
                        options={options}
                        selectHintOnEnter={true}
                        minLength={0}
                        newSelectionPrefix="Add a new item: "
                        ignoreDiacritics={false}
                        selected={this.state.selected}
                        filterBy={filter}
                        onChange={(selected) => {
                            this.handleOnchange({ selected });
                        }}
                        placeholder={placeholder}
                    />
                :<Typeahead
                        key={key}
                        id={id}
                        allowNew = {allowNew}
                        clearButton
                        bsSize="small"
                        labelKey={option => `${option.value}`}
                        multiple
                        options={this.props.showList}
                        selectHintOnEnter={true}
                        highlightOnlyResult={true}
                        minLength={0}
                        newSelectionPrefix="Select item: "
                        ignoreDiacritics={false}
                        selected={this.state.selected}
                        onChange={(selected) => {
                            this.handleOnchange({ selected });
                        }}
                        placeholder={placeholder}
                    />} 
                </Col>
            </Row>
        )
    }
}
const mapDipatchToProps = dispatch => {
    return {
        ChangeReduxFormAction : (obj)=>{
            dispatch(ChangeReduxFormAction(obj));
        } 
    }
  }
  
  const mapStateToProps = state => {
    return {
     showList : state.showTimeList && state.showTimeList.showTimeList ? state.showTimeList.showTimeList : []
    }
  }
  
export default connect(mapStateToProps, mapDipatchToProps)(TypeHeadInputComponent);