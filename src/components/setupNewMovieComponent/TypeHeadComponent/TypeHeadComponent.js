import React, { Component } from 'react';
import TypeHeadInputComponent from './TypeHeadInputComponent/TypeHeadInputComponent';

class TypeHeadComponent extends Component {
    render() {

        return (
            <div>
                {this.props.typehead.map(typehead => (
                    <div key={typehead.name}>
                    <TypeHeadInputComponent
                        key={typehead.name}
                        typehead={typehead} 
                        form={this.props.form}/>
                    </div>
                ))}
            </div>
        )
    }
}


export default TypeHeadComponent;