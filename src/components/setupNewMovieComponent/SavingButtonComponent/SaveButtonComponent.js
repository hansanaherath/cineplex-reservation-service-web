import React, { Component } from "react";
import { Button, Row, Col } from "react-bootstrap";
import { FaSave } from "react-icons/fa";

class SaveButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <Row>
                <Col md="9"></Col>
                <Col md="3">
                    <div>
                        <Button type="button" className="btn-primary custom-btn" onClick={() => { this.props.save() }}><FaSave className="add-button-icon"/>Save</Button>
                    </div>
                </Col>
            </Row>
        )
    }
}

export default SaveButtonComponent;