import React, { Component } from 'react';
import { Modal, Row, Button, Table } from 'react-bootstrap';

class ShowingSeatList extends Component {
    render() {

        return (
            <Modal className="show-information-details-model"
                show={this.props.show}
                onHide={false}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Seat Reservation Details
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Table>
                            <thead>
                                <th>Available Seat Numbers</th>
                                <th>Received Seat Numbers</th>
                                <th>Total</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{this.props.availableList}</td>
                                    <td>{this.props.receivedList}</td>
                                    <td>{this.props.total}</td></tr>
                            </tbody>
                        </Table>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="custom-btn" variant="outline-danger" style={{ width: '80px' }} onClick={this.props.setModalHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}


export default ShowingSeatList;