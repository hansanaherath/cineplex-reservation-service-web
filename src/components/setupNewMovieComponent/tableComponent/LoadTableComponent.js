import React, { Component } from "react";
import TableHeadComponent from "../../commonComponents/TableHeadComponent";
import TableBodyComponent from "../../commonComponents/TableBodyComponent";
import { Table } from "react-bootstrap";
import ShowingSeatList from "./ShowingSeatList/ShowingSeatList";

class LoadTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ["Name", "Description", "Show Times", "Action"],
            modalShow: false,
            availableList: '',
            receivedList: '',
            total: 0
        }
    }

    remove = (e, gridArray, deleteArray) => {
        let id = e.target.id.split("@@@")[0];
        let index = e.target.id.split("@@@")[1];

        if (id === null) {
            gridArray.splice(index, 1);
        } else {
            deleteArray.push(gridArray[index])
            gridArray.splice(index, 1)
        }
        this.props.remove(gridArray, deleteArray)
    }

    edit = (e, gridArray) => {
        let index = e.target.id.split("@@@")[1];
        let id = e.target.id.split("@@@")[0];

        let editObj = gridArray[index]

        this.props.edit(editObj, index, gridArray)
    }

    showTimesFunction = (j, seatArray) => {

    }

    setModalShow = (e, gridArray) => {
        let dataGridArray = [...gridArray]
        let movieObjindex = e.target.id.split("@@@")[1];
        let id = e.target.id.split("@@@")[0];
        let showTimeObjIndex = e.target.id.split("@@@")[2];
        let MovieObj = dataGridArray[movieObjindex]
        let showTimesEntityList = MovieObj.hasOwnProperty("showTimesEntityList") ? MovieObj.showTimesEntityList : MovieObj.showTimeEntityList
        let showTimeObj = showTimesEntityList[showTimeObjIndex]
        let seatObjList = showTimeObj.hasOwnProperty("seatList") ? showTimeObj.seatList : showTimeObj.seatsEntityList
        let receivedList = ''
        let availableList = ''
        let total = 0

        seatObjList.length > 0 ? seatObjList.forEach(element => {
            if (element.status === 'Y') {
                total += 1
                element.isReserved === 'N' ? (availableList += (element.seatNumber + " , ")) : (receivedList += (element.seatNumber + " , "))
            }
        }) : null
        this.setState({
            modalShow: true,
            receivedList: receivedList,
            availableList: availableList,
            total: total
        })
    }

    setModalHide = () => {
        this.setState({
            modalShow: false
        })
    }

    render() {
        const { tableHead, availableList, receivedList, total } = this.state;
        const { gridArray, deleteArray } = this.props
        return (
            <div className="data-table-responsive">
                <Table striped bordered hover className="assign-combination-table add-new-movie-table-main mt-5">
                    <thead>
                        <TableHeadComponent
                            dataArray={tableHead}
                        />
                    </thead>
                    <TableBodyComponent
                        dataArray={gridArray}
                        remove={(e) => this.remove(e, gridArray, deleteArray)}
                        edit={(e) => this.edit(e, gridArray)}
                        showTimesFunction={(e) => this.setModalShow(e, gridArray)}
                    />
                </Table>

                <ShowingSeatList
                    show={this.state.modalShow}
                    setModalHide={this.setModalHide}
                    availableList={availableList}
                    receivedList={receivedList}
                    total={total}
                />
            </div>
        )
    }
}

export default LoadTableComponent;