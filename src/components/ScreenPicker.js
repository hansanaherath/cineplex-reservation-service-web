import React, { Component } from 'react';
import { connect } from 'react-redux';
import ScreenPickerComponent from './ScreenPickerComponent'
import { FormDataAction } from '../redux/actions/FormDataAction';

class ScreenPicker extends Component {

  constructor(props) {
    super(props);
    let url = new URL(window.location.href)
    this.state = {
      screen: url.searchParams.get("screen"),
    }
  }

  componentDidMount() {
    this.props.FormDataAction();
  }

  render() {
    return (
      <ScreenPickerComponent
        screen={this.state.screen}
      />
    );
  }
}

const mapDipatchToProps = dispatch => {
  return {
    FormDataAction: () => {
      dispatch(FormDataAction({
        "textFieldComponent": [{
          "name": "Movie Name",
          "type": "text",
          "id": "moviename",
          "value": "",
          "placehoder": "Movie Name",
          "className": ""
        },
        {
          "name": "Description",
          "type": "textarea",
          "id": "description",
          "value": "",
          "placehoder": "Description",
          "className": ""
        }
        ],

        "typehead": [{
          "id": "showtime",
          "options": [{
            "id": 1,
            "value": "10.30AM"
          },
          {
            "id": 2,
            "value": "01.30PM"
          },
          {
            "id": 3,
            "value": "07.30PM"
          }],
          "selected": [],
          "placeholder": "Enter Time",
          "filter": ["showTime"],
          "allowNew": false,
          "label": "Show Times"
        }, {
          "id": "seats",
          "options": [],
          "selected": [],
          "placeholder": "Type Seat Number",
          "filter": [],
          "allowNew": true,
          "label": "Seat Ids"
        }]
      }
      ));
    }
  }
}

const mapStateToProps = state => {
  return {
  }
}

export default connect(mapStateToProps, mapDipatchToProps)(ScreenPicker);