import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

class MovieImageComponent extends Component {
    render() {
        const {image} = this.props;
        return (
            <div className="movie-image-main">
                <img src={image} alt="product" />
            </div>
        )
    }
}


export default MovieImageComponent;