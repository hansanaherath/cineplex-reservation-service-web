import React, { Component } from 'react';
import { Col, Button } from 'react-bootstrap';
import { updateSeatObjAction } from '../../../../../../../redux/actions/updateSeatObjAction';
import { connect } from 'react-redux';

class SeatComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReservedSeat : ''
        }
      }
    
    changeButtonState =(e) =>{
        const { id, isReserved, seatNumber, showTimeId, status } = this.props;
        let reserved = this.state.isReservedSeat;
        this.setState({
            isReservedSeat : reserved ===  'Y' ? "N" :"Y"
        })
        this.props.updateSeatObj(id);
    }

    static getDerivedStateFromProps(props, state){
        if('' === state.isReservedSeat){
            return({
                isReservedSeat : props.isReserved
            })
        }
    }

    render() {
        const { id, isReserved, seatNumber, showTimeId, status } = this.props;
        return (
            <Button className={this.state.isReservedSeat === 'Y' ? "recieved-seat" : "unrecieved-seat"} onClick={(e) =>{this.changeButtonState(e)}}>{seatNumber}</Button>
        )
    }
}

const mapDipatchToProps = dispatch => {
    return {
        updateSeatObj: (id) => {
        dispatch(updateSeatObjAction(id));
    }
    }
  }
  
  const mapStateToProps = state => {
    return {
    }
  }
  
  export default connect(mapStateToProps, mapDipatchToProps)(SeatComponent);