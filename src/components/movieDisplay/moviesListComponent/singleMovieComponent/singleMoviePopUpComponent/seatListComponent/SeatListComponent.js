import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import SeatComponent from './seatComponent/SeatComponent';

class SeatListComponent extends Component {

    changeState = () =>{

    }

    render() {
        //{showTimeId, isReserved, id, seatNumber, status, Y} 
        let seatList = [...this.props.seatList];
        return (
            <div className="seat-plan">
                <h6>Select your Seat</h6>
            <div className="seat-component-main">
                {seatList.length > 0 ? seatList.map((item) => (
                    <div className="seat-button-main">
                        <SeatComponent
                         {...item}/>
                    </div>

                )) : null}
                </div>
            </div>
        )
    }
}


export default SeatListComponent;