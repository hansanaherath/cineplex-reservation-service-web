import React, { Component } from 'react';
import { Form, FormGroup, Col, Button, Row, Modal } from "react-bootstrap";
//import { GetTotalPriceAction, resetFetchingTotalPrice } from '../../../../../redux/actions/GetViewMovieListAction';
import { connect } from 'react-redux';
import MovieImageComponent from '../../../movieImageComponent/MovieImageComponent';
import SeatListComponent from './seatListComponent/SeatListComponent';

class SingleMoviePopUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      itemCount: 1,
      id: this.props.id,
      singleUnitPrice: this.props.singleUnitPrice,
      actualPrice: 0,
      total: this.props.fetchTotalPrice && this.props.fetchTotalPrice.fetchTotalPrice && this.props.fetchTotalPrice.fetchTotalPrice >= 0 ? this.props.fetchTotalPrice.fetchTotalPrice : this.props.singleUnitPrice
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.fetchTotalPrice && props.fetchTotalPrice.fetchTotalPrice && props.fetchTotalPrice.fetchTotalPrice > 0 && props.fetchTotalPrice.fetchTotalPrice != state.actualPrice) {
      return {
        actualPrice: props.fetchTotalPrice.fetchTotalPrice
      }
    }
    return null;
  }

  setModalHide = () => {
    //this.props.onHide()
  }

  GetTotalPriceAction = (val, id) => {
    this.setState({
      itemCount: val,
      id: id
    })
    this.props.GetTotalPriceAction(val, id)
  }

  addToCart = () => {
    this.props.GetTotalPriceAction(this.state.itemCount, this.state.id)
    let obj = {
      "id": this.state.id,
      "itemCount": this.state.itemCount,
      "totalPrice": this.state.singleUnitPrice * this.state.itemCount,
      "discount": (this.state.singleUnitPrice * this.state.itemCount) - this.state.total,
      "grandTotal": this.state.actualPrice
    }
    this.props.addToCart(obj)
  }

  clearCart = () => { this.props.clearCart() }

  actualPrice = (price) => { this.setState({ actualPrice: price.toFixed(3) }) }

  render() {
    const { description, image, id, name, showTimeEntityList } = this.props
    let ShowTimeObj = {}
    showTimeEntityList.forEach(element => {
      if (element.id = id) {
        ShowTimeObj = element
      }
    });
    return (

      <Modal
        show={this.props.show}
        onHide={false}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Check Out Your Seat
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col lg={4}><MovieImageComponent image={image} /></Col>
            <Col lg={8}>
              <Row>
                <div>
                  <h4>{name}</h4>
                  <p>{description}</p>
                </div>

                <SeatListComponent
                  seatList={ShowTimeObj.hasOwnProperty("seatList") ? ShowTimeObj.seatList : []}

                />
              </Row>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button className="custom-btn" variant="outline-danger" style={{ width: '80px' }} onClick={this.props.setModalHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );

  }
}

const mapDipatchToProps = dispatch => {
  return {
    /* GetTotalPriceAction: (val, id) => {
      dispatch(GetTotalPriceAction(val, id));
    }, */
    /* resetGetTotalPriceList: () => {
      dispatch(resetFetchingTotalPrice());
    } */
  }
}

const mapStateToProps = state => {
  return {
    fetchTotalPrice: state.fetchTotalPrice
  }
}

export default connect(mapStateToProps, mapDipatchToProps)(SingleMoviePopUp);
