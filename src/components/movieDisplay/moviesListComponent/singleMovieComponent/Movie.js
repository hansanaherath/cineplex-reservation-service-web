import React, { Component } from 'react';
import { Form, FormGroup, Col, Button, Row, Card } from "react-bootstrap";
import PropTypes from 'prop-types';
import MovieImageComponent from '../../movieImageComponent/MovieImageComponent';
import SingleMoviePopUp from './singleMoviePopUpComponent/SingleMoviePopUp';

class Movie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalShow: false,
            id : null
        }
    }

    setModalShow = (e) => {
        this.setState({
            modalShow: true,
            id : e.target.id
        })
    }

    setModalHide = () => {
        this.setState({
            modalShow: false
        })
    }

    render() {
        const { name, description, showTimeEntityList, image } = this.props;

        return (
            <div  className="product_thumb">
        
                 
                        <div className="sigle-movie">
                            <MovieImageComponent
                                image={image} />
                            <div  className="product-details" >
                                <div className="movie-description">
                                    <h4>{name}</h4>
                                <p className="product__price" >{description}</p>
                                </div>
                                <div className="show-time-main">
                                {showTimeEntityList.length > 0 ? showTimeEntityList.map(item => (
                                    
                                    <Button  className="block" onClick={(e) =>{this.setModalShow(e)}} id={item.id}>{item.showTime}</Button>
                                )) : null}
</div>
                            </div>
                        </div>
             
                    <SingleMoviePopUp
                        show={this.state.modalShow}
                        id={this.state.id}
                        setModalHide={this.setModalHide}
                        //addToCart={this.props.addToCart}
                        //clearCart={this.props.clearCart}
                        {...this.props}
                    />
      
            </div>
        );
    }
}

export default Movie;