import React, { Component } from 'react';
import { Form, FormGroup, Col, Button, Row, Card } from "react-bootstrap";
import PropTypes from 'prop-types';
import MovieImageComponent from '../../movieImageComponent/MovieImageComponent';
import SingleMoviePopUp from '../singleMovieComponent/singleMoviePopUpComponent/SingleMoviePopUp';

class Movie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalShow: false,
            id : null
        }
    }

    setModalShow = (e) => {
        this.setState({
            modalShow: true,
            id : e.target.id
        })
    }

    setModalHide = () => {
        this.setState({
            modalShow: false
        })
    }

    render() {
        const { name, description, showTimeEntityList, image } = this.props;

        return (
            <div /* className="product_thumb" */>
                <div /* className="caption" */>
                    <Card /* style={{ width: '19rem' }} */>
                        <Card.Body>
                            <MovieImageComponent
                                image={image} />
                            <div /* className="product-details" */>
                                <h3>{name}</h3>
                                <p /* className="product__price" */>{description}</p><br />
                                {showTimeEntityList.length > 0 ? showTimeEntityList.map(item => (
                                    <Button /* className="block" */ onClick={(e) =>{this.setModalShow(e)}} id={item.id}>{item.showTime}</Button>
                                )) : null}

                            </div>
                        </Card.Body>
                    </Card>
                    <SingleMoviePopUp
                        show={this.state.modalShow}
                        id={this.state.id}
                        setModalHide={this.setModalHide}
                        //addToCart={this.props.addToCart}
                        //clearCart={this.props.clearCart}
                        {...this.props}
                    />
                </div>
            </div>
        );
    }
}

export default Movie;