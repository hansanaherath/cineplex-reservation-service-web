import React, { Component } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import Movie from './singleMovieComponent/Movie';

class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      cartArray: [],
      totalItem: 0,
      subTotal: 0,
      delivaryCharge: 0,
      discount: 0,
      grandTotal: 0
    }
  }

  render() {
    const [...movieList] = this.props.movieList
    return (
      <div>
        <Container fluid>
          <Row>
            <div className = "main-container">
            <Col lg={12}>
              <h2 className="display-movie-heading">Movies</h2>
              <ul className="movie-main-component">
                {movieList.map(movie => (
                  <li className="movie-sub-component" key={movie.id}>
                    <Movie {...movie}
                      image={"https://images.pexels.com/photos/1351238/pexels-photo-1351238.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"} />
                  </li>
                ))}
              </ul>
            </Col>
            </div>
          </Row>
        </Container>
      </div>
    )
  }
}


export default MovieList;