import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { FaEdit,FaTrashAlt } from "react-icons/fa";

const TableBodyComponent = (props) => {
    let array = props && props.dataArray.length > 0 ? props.dataArray : []
    let tableData = array.map((item, i) => {
        let showTimesEntityList = item.hasOwnProperty("showTimesEntityList") ? item.showTimesEntityList : item.showTimeEntityList
        return (
            <tr key={i}>
                <td>{item.name}</td>
                <td>{item.description}</td>
                <td>{showTimesEntityList.length > 0 ? showTimesEntityList.map((element, j) => (
                    <Button key={j} id={item.id + "@@@" + i+ "@@@" +j} onClick={props.showTimesFunction}>{element.showTimeOptionValue}</Button>
                )) : null
                }</td>
                <td><Button id={item.id + "@@@" + i} onClick={props.edit} variant="primary" size="sm"><FaEdit className="add-button-icon"/>Edit</Button>
                    <Button id={item.id + "@@@" + i} onClick={props.remove} variant="danger" size="sm"><FaTrashAlt className="add-button-icon"/>Remove</Button></td>
            </tr>
        )
    })
    return (
        <tbody>{tableData}</tbody>);
}

export default TableBodyComponent;