import React, { Component } from "react";
import { connect } from 'react-redux';
import SetupNewMovie from "./setupNewMovieComponent/SetupNewMovieMainComponent";
import { GetMovieListAction } from "../redux/actions/GetMovieListAction";
import { GetViewMovieListAction } from "../redux/actions/GetViewMovieListAction";
import { GetShowTimeListAction } from "../redux/actions/GetShowTimeListAction";
import MovieList from "./movieDisplay/moviesListComponent/MovieList";

class ScreenPickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: this.props.screen,
      movieList : [{"name":"Hansana","showTimeEntityList":[{"showTime":"10.30AM","showTimeOptionId":1,"movieId":40,"seatList":[{"showTimeId":63,"isReserved":"N","id":69,"seatNumber":12,"status":"Y"},{"showTimeId":63,"isReserved":"N","id":70,"seatNumber":23,"status":"Y"}],"id":63,"status":"Y"},{"showTime":"02.30PM","showTimeOptionId":3,"movieId":40,"seatList":[{"showTimeId":64,"isReserved":"Y","id":71,"seatNumber":12,"status":"Y"},{"showTimeId":64,"isReserved":"N","id":72,"seatNumber":23,"status":"Y"}],"id":64,"status":"Y"}],"description":"Rik@2Han","id":40,"status":"Y"},{"name":"ewe","showTimeEntityList":[{"showTime":"10.30AM","showTimeOptionId":1,"movieId":45,"seatList":[{"showTimeId":69,"isReserved":"N","id":77,"seatNumber":23,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":78,"seatNumber":32,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":79,"seatNumber":32,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":80,"seatNumber":43,"status":"Y"}],"id":69,"status":"Y"},{"showTime":"02.30PM","showTimeOptionId":3,"movieId":45,"seatList":[{"showTimeId":70,"isReserved":"N","id":81,"seatNumber":23,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":82,"seatNumber":32,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":83,"seatNumber":32,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":84,"seatNumber":43,"status":"Y"}],"id":70,"status":"Y"},{"showTime":"07.00PM","showTimeOptionId":4,"movieId":45,"seatList":[{"showTimeId":71,"isReserved":"N","id":86,"seatNumber":32,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":87,"seatNumber":32,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":88,"seatNumber":43,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":85,"seatNumber":23,"status":"Y"}],"id":71,"status":"Y"}],"description":"eweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee","id":45,"status":"Y"},{"name":"ewee","showTimeEntityList":[{"showTime":"10.30AM","showTimeOptionId":1,"movieId":46,"seatList":[{"showTimeId":72,"isReserved":"N","id":89,"seatNumber":32,"status":"Y"}],"id":72,"status":"Y"},{"showTime":"02.30PM","showTimeOptionId":3,"movieId":46,"seatList":[{"showTimeId":73,"isReserved":"N","id":90,"seatNumber":32,"status":"Y"}],"id":73,"status":"Y"},{"showTime":"10.00PM","showTimeOptionId":5,"movieId":46,"seatList":[{"showTimeId":74,"isReserved":"N","id":91,"seatNumber":32,"status":"Y"}],"id":74,"status":"Y"}],"description":"ewewww","id":46,"status":"Y"},{"name":"ewd","showTimeEntityList":[{"showTime":"10.30AM","showTimeOptionId":1,"movieId":47,"seatList":[{"showTimeId":75,"isReserved":"N","id":92,"seatNumber":233,"status":"Y"}],"id":75,"status":"Y"},{"showTime":"02.30PM","showTimeOptionId":3,"movieId":47,"seatList":[{"showTimeId":76,"isReserved":"N","id":93,"seatNumber":233,"status":"Y"}],"id":76,"status":"Y"}],"description":"ddddddddddddddddddddddddddddddddd","id":47,"status":"Y"},{"name":"test","showTimeEntityList":[{"showTime":"10.30AM","showTimeOptionId":1,"movieId":48,"seatList":[{"showTimeId":77,"isReserved":"N","id":94,"seatNumber":12,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":95,"seatNumber":22,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":96,"seatNumber":32,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":97,"seatNumber":43,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":98,"seatNumber":555,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":99,"seatNumber":444,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":100,"seatNumber":54,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":101,"seatNumber":454,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":102,"seatNumber":545,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":103,"seatNumber":545,"status":"Y"}],"id":77,"status":"Y"},{"showTime":"02.30PM","showTimeOptionId":3,"movieId":48,"seatList":[{"showTimeId":78,"isReserved":"N","id":104,"seatNumber":12,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":105,"seatNumber":22,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":106,"seatNumber":32,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":107,"seatNumber":43,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":108,"seatNumber":555,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":109,"seatNumber":444,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":110,"seatNumber":54,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":111,"seatNumber":454,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":112,"seatNumber":545,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":113,"seatNumber":545,"status":"Y"}],"id":78,"status":"Y"},{"showTime":"10.00PM","showTimeOptionId":5,"movieId":48,"seatList":[{"showTimeId":79,"isReserved":"N","id":114,"seatNumber":12,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":115,"seatNumber":22,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":116,"seatNumber":32,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":117,"seatNumber":43,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":118,"seatNumber":555,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":119,"seatNumber":444,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":120,"seatNumber":54,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":121,"seatNumber":454,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":122,"seatNumber":545,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":123,"seatNumber":545,"status":"Y"}],"id":79,"status":"Y"}],"description":"dadsczx dasdas dsad sasd sdasd sdasdsa","id":48,"status":"Y"}]
      ,list : {"data":[{"name":"Hansana","showTimeEntityList":[{"showTime":1,"movieId":40,"seatList":[{"showTimeId":63,"isReserved":"N","id":69,"seatNumber":12,"status":"Y"},{"showTimeId":63,"isReserved":"N","id":70,"seatNumber":23,"status":"Y"}],"id":63,"showTimeOptionValue":"10.30AM","status":"Y"},{"showTime":3,"movieId":40,"seatList":[{"showTimeId":64,"isReserved":"Y","id":71,"seatNumber":12,"status":"Y"},{"showTimeId":64,"isReserved":"N","id":72,"seatNumber":23,"status":"Y"}],"id":64,"showTimeOptionValue":"02.30PM","status":"Y"}],"description":"Rik@2Han","id":40,"status":"Y"},{"name":"trt","showTimeEntityList":[{"showTime":1,"movieId":44,"seatList":[{"showTimeId":68,"isReserved":"N","id":76,"seatNumber":43,"status":"Y"}],"id":68,"showTimeOptionValue":"10.30AM","status":"Y"}],"description":"trtr","id":44,"status":"N"},{"name":"ewe","showTimeEntityList":[{"showTime":1,"movieId":45,"seatList":[{"showTimeId":69,"isReserved":"N","id":77,"seatNumber":23,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":78,"seatNumber":32,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":79,"seatNumber":32,"status":"Y"},{"showTimeId":69,"isReserved":"N","id":80,"seatNumber":43,"status":"Y"}],"id":69,"showTimeOptionValue":"10.30AM","status":"Y"},{"showTime":3,"movieId":45,"seatList":[{"showTimeId":70,"isReserved":"N","id":81,"seatNumber":23,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":82,"seatNumber":32,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":83,"seatNumber":32,"status":"Y"},{"showTimeId":70,"isReserved":"N","id":84,"seatNumber":43,"status":"Y"}],"id":70,"showTimeOptionValue":"02.30PM","status":"Y"},{"showTime":4,"movieId":45,"seatList":[{"showTimeId":71,"isReserved":"N","id":86,"seatNumber":32,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":87,"seatNumber":32,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":88,"seatNumber":43,"status":"Y"},{"showTimeId":71,"isReserved":"N","id":85,"seatNumber":23,"status":"Y"}],"id":71,"showTimeOptionValue":"07.00PM","status":"Y"}],"description":"eweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee","id":45,"status":"Y"},{"name":"ewee","showTimeEntityList":[{"showTime":1,"movieId":46,"seatList":[{"showTimeId":72,"isReserved":"N","id":89,"seatNumber":32,"status":"Y"}],"id":72,"showTimeOptionValue":"10.30AM","status":"Y"},{"showTime":3,"movieId":46,"seatList":[{"showTimeId":73,"isReserved":"N","id":90,"seatNumber":32,"status":"Y"}],"id":73,"showTimeOptionValue":"02.30PM","status":"Y"},{"showTime":5,"movieId":46,"seatList":[{"showTimeId":74,"isReserved":"N","id":91,"seatNumber":32,"status":"Y"}],"id":74,"showTimeOptionValue":"10.00PM","status":"Y"}],"description":"ewewww","id":46,"status":"Y"},{"name":"ewd","showTimeEntityList":[{"showTime":1,"movieId":47,"seatList":[{"showTimeId":75,"isReserved":"N","id":92,"seatNumber":233,"status":"Y"}],"id":75,"showTimeOptionValue":"10.30AM","status":"Y"},{"showTime":3,"movieId":47,"seatList":[{"showTimeId":76,"isReserved":"N","id":93,"seatNumber":233,"status":"Y"}],"id":76,"showTimeOptionValue":"02.30PM","status":"Y"}],"description":"ddddddddddddddddddddddddddddddddd","id":47,"status":"Y"},{"name":"test","showTimeEntityList":[{"showTime":1,"movieId":48,"seatList":[{"showTimeId":77,"isReserved":"N","id":94,"seatNumber":12,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":95,"seatNumber":22,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":96,"seatNumber":32,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":97,"seatNumber":43,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":98,"seatNumber":555,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":99,"seatNumber":444,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":100,"seatNumber":54,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":101,"seatNumber":454,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":102,"seatNumber":545,"status":"Y"},{"showTimeId":77,"isReserved":"N","id":103,"seatNumber":545,"status":"Y"}],"id":77,"showTimeOptionValue":"10.30AM","status":"Y"},{"showTime":3,"movieId":48,"seatList":[{"showTimeId":78,"isReserved":"N","id":104,"seatNumber":12,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":105,"seatNumber":22,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":106,"seatNumber":32,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":107,"seatNumber":43,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":108,"seatNumber":555,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":109,"seatNumber":444,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":110,"seatNumber":54,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":111,"seatNumber":454,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":112,"seatNumber":545,"status":"Y"},{"showTimeId":78,"isReserved":"N","id":113,"seatNumber":545,"status":"Y"}],"id":78,"showTimeOptionValue":"02.30PM","status":"Y"},{"showTime":5,"movieId":48,"seatList":[{"showTimeId":79,"isReserved":"N","id":114,"seatNumber":12,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":115,"seatNumber":22,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":116,"seatNumber":32,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":117,"seatNumber":43,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":118,"seatNumber":555,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":119,"seatNumber":444,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":120,"seatNumber":54,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":121,"seatNumber":454,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":122,"seatNumber":545,"status":"Y"},{"showTimeId":79,"isReserved":"N","id":123,"seatNumber":545,"status":"Y"}],"id":79,"showTimeOptionValue":"10.00PM","status":"Y"}],"description":"dadsczx dasdas dsad sasd sdasd sdasdsa","id":48,"status":"Y"}]},
      data: [{
        "name": "ewewe",
        "showTimeEntityList": [
          {
            "showTime": "10.30AM",
            "movieId": 19,
            "seatList": [
              {
                "showTimeId": 24,
                "isReserved": "Y",
                "id": 18,
                "seatNumber": 5,
                "status": "Y"
              }
            ],
            "id": 24,
            "status": "Y"
          },
          {
            "showTime": "10.30AM",
            "movieId": 19,
            "seatList": [
              {
                "showTimeId": 24,
                "isReserved": "Y",
                "id": 18,
                "seatNumber": 5,
                "status": "Y"
              }
            ],
            "id": 24,
            "status": "Y"
          }
        ],
        "description": "This movie is very beautiful amdnsdjdnj sdsa dsdfd dsasd asddas dsds ssew kkj.",
        "id": 19,
        "status": "N"
      }]
    };
    this.ShowView = this.ShowView.bind(this);
  }

  componentDidMount() {
    this.props.GetViewMovieListAction();
    this.props.GetMovieListAction();
    this.props.GetShowTimeListAction();
  }

  ShowView = view => {
    switch (view) {
      case "SetupNewMovie":
        return <SetupNewMovie
          fetchMovieList={this.props.fetchMovieList}
          //fetchMovieList={this.state.list}
        />
      case "movieList":
        return <MovieList
          movieList={this.props.fetchViewMovieList}
          //movieList = {this.state.movieList}
        />
      default:
        return "PAGE NOT FOUND"
    }
  };


  render() {
    let view = this.state.screen;
    return <div className="main-container">{this.ShowView(view)}
    </div>;
  }
}

const mapDipatchToProps = dispatch => {
  return {
    GetMovieListAction: () => {
      dispatch(GetMovieListAction());
    },
    GetViewMovieListAction: () => {
      dispatch(GetViewMovieListAction());
    },
    GetShowTimeListAction: () => {
      dispatch(GetShowTimeListAction());
    }
  }
}

const mapStateToProps = state => {
  return {
    fetchMovieList: state.fetchMovieList && state.fetchMovieList.fetchMovieList && state.fetchMovieList.fetchMovieList ? state.fetchMovieList.fetchMovieList : {},
    fetchViewMovieList : state.fetchViewMovieList && state.fetchViewMovieList.fetchViewMovieList && state.fetchViewMovieList.fetchViewMovieList.data ? state.fetchViewMovieList.fetchViewMovieList.data : [] 
  }
}

export default connect(mapStateToProps, mapDipatchToProps)(ScreenPickerComponent);
